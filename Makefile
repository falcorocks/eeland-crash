mypy:
	mypy --strict --ignore-missing-imports eeland

clean:
	black .
	isort --profile=black .

auto: format mypy

NOW=$(shell date +%s)
1MINUTEAGO=$(shell expr $(NOW) - 60)
1hLATER=$(shell expr $(NOW) + 3600)
2hLATER=$(shell expr $(NOW) + 7200)

test-now:
	eeland --index dns --range-start-epochs=0 --range-end-epochs=$(NOW)

test-1h:
	eeland --index dns --range-start-epochs=0 --range-end-epochs=$(1hLATER)

test-2h:
	eeland --index dns --range-start-epochs=0 --range-end-epochs=$(2hLATER)

test-minute:
	eeland --index dns --range-start-epochs=$(1MINUTEAGO) --range-end-epochs=$(NOW)

test-minute-csv:
	eeland --index dns --range-start-epochs=$(1MINUTEAGO) --range-end-epochs=$(NOW) --to-csv test.csv

test-all:
	eeland --index dns
