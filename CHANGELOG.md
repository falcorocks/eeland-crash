## [2.0.2](https://gitlab.com/notno/eeland/compare/v2.0.1...v2.0.2) (2021-11-02)


### Bug Fixes

* twine pypi url ([1a7b997](https://gitlab.com/notno/eeland/commit/1a7b997d7570f213eebe40948e27f487712f81cd))

## [2.0.2](https://gitlab.com/notno/eeland/compare/v2.0.1...v2.0.2) (2021-11-02)


### Bug Fixes

* twine pypi url ([1a7b997](https://gitlab.com/notno/eeland/commit/1a7b997d7570f213eebe40948e27f487712f81cd))

## [2.0.2](https://gitlab.com/notno/eeland/compare/v2.0.1...v2.0.2) (2021-11-02)


### Bug Fixes

* twine pypi url ([1a7b997](https://gitlab.com/notno/eeland/commit/1a7b997d7570f213eebe40948e27f487712f81cd))

## [2.0.2](https://gitlab.com/notno/eeland/compare/v2.0.1...v2.0.2) (2021-11-02)


### Bug Fixes

* twine pypi url ([1a7b997](https://gitlab.com/notno/eeland/commit/1a7b997d7570f213eebe40948e27f487712f81cd))

## [2.0.2](https://gitlab.com/notno/eeland/compare/v2.0.1...v2.0.2) (2021-11-02)


### Bug Fixes

* twine pypi url ([1a7b997](https://gitlab.com/notno/eeland/commit/1a7b997d7570f213eebe40948e27f487712f81cd))

## [2.0.2](https://gitlab.com/notno/eeland/compare/v2.0.1...v2.0.2) (2021-11-02)


### Bug Fixes

* twine pypi url ([1a7b997](https://gitlab.com/notno/eeland/commit/1a7b997d7570f213eebe40948e27f487712f81cd))

## [2.0.2](https://gitlab.com/notno/eeland/compare/v2.0.1...v2.0.2) (2021-11-02)


### Bug Fixes

* twine pypi url ([1a7b997](https://gitlab.com/notno/eeland/commit/1a7b997d7570f213eebe40948e27f487712f81cd))

## [2.0.1](https://gitlab.com/notno/eeland/compare/v2.0.0...v2.0.1) (2021-11-02)


### Bug Fixes

* fixes broken poetry core version ([99e48d5](https://gitlab.com/notno/eeland/commit/99e48d5afde8e9d7c08fcdaaebde20dc20375298))

# [2.0.0](https://gitlab.com/notno/eeland/compare/v1.0.1...v2.0.0) (2021-11-02)


* fix!: splits elastic bootstrap and init functions ([08a9c3c](https://gitlab.com/notno/eeland/commit/08a9c3cb860fe298363f186cfa0975ddee5f9da2))


### BREAKING CHANGES

* changes public functions names and signatures

# [2.0.0-alpha.1](https://gitlab.com/notno/eeland/compare/v1.0.1...v2.0.0-alpha.1) (2021-11-02)


### Bug Fixes

* splits elastic bootstrap and init functions ([5bf5545](https://gitlab.com/notno/eeland/commit/5bf5545a3594a089b4b7955635060b1b013440db))


### BREAKING CHANGES

* changes public functions names and signature

## [1.0.1](https://gitlab.com/notno/eeland/compare/v1.0.0...v1.0.1) (2021-10-01)


### Bug Fixes

* add timezone info in conversion ([1766b0a](https://gitlab.com/notno/eeland/commit/1766b0ae5615271e27c323b2d529542b00cced0a))

# [1.0.0](https://gitlab.com/notno/eeland/compare/v0.1.13...v1.0.0) (2021-09-22)


### Bug Fixes

* allow grype to fail ([615eb19](https://gitlab.com/notno/eeland/commit/615eb1913e2a4a38db74f309696d6e0cb27b8f27))
* changes wrappers submodule name to utils ([8a76373](https://gitlab.com/notno/eeland/commit/8a76373d310ee7eaee2ad5161712a8c18801ff0c))
* disable bandit false positive ([3d62584](https://gitlab.com/notno/eeland/commit/3d62584e64c72e447c06d54f4027ec115e116032))
* **Dockerfile:** adds base image security updates ([247dcd7](https://gitlab.com/notno/eeland/commit/247dcd7169fc74727cf6a115ad4efe882db38ec0))
* **mypy:** fixes types ([0e8a4a7](https://gitlab.com/notno/eeland/commit/0e8a4a7c0c164a1f069ea12edfca236ae4fa1c1b))
* stick to http and disable https checks ([603cccc](https://gitlab.com/notno/eeland/commit/603cccc6d49193f74b815137d7bba1717a177c83))
* switches to ci/templates beta ([8c90888](https://gitlab.com/notno/eeland/commit/8c9088886c93296092a6d96bb95a799f831eeafe))
* updates deps ([c818008](https://gitlab.com/notno/eeland/commit/c8180084e8997ce3f7ba99c4ecac94a92c8ca4e1))
* updates versions manually ([4ac59e8](https://gitlab.com/notno/eeland/commit/4ac59e87847820c36d93520781dd5b329bc398a9))


### BREAKING CHANGES

* will break imports

# [1.0.0-beta.2](https://gitlab.com/notno/eeland/compare/v1.0.0-beta.1...v1.0.0-beta.2) (2021-09-22)


### Bug Fixes

* stick to http and disable https checks ([603cccc](https://gitlab.com/notno/eeland/commit/603cccc6d49193f74b815137d7bba1717a177c83))
* updates versions manually ([4ac59e8](https://gitlab.com/notno/eeland/commit/4ac59e87847820c36d93520781dd5b329bc398a9))

# [1.0.0-beta.1](https://gitlab.com/notno/eeland/compare/v0.1.14-beta.1...v1.0.0-beta.1) (2021-09-08)


### Bug Fixes

* changes wrappers submodule name to utils ([8a76373](https://gitlab.com/notno/eeland/commit/8a76373d310ee7eaee2ad5161712a8c18801ff0c))
* updates deps ([c818008](https://gitlab.com/notno/eeland/commit/c8180084e8997ce3f7ba99c4ecac94a92c8ca4e1))


### BREAKING CHANGES

* will break imports

## [0.1.14-beta.1](https://gitlab.com/notno/eeland/compare/v0.1.13...v0.1.14-beta.1) (2021-09-08)


### Bug Fixes

* **mypy:** fixes types ([0e8a4a7](https://gitlab.com/notno/eeland/commit/0e8a4a7c0c164a1f069ea12edfca236ae4fa1c1b))
* allow grype to fail ([615eb19](https://gitlab.com/notno/eeland/commit/615eb1913e2a4a38db74f309696d6e0cb27b8f27))
* disable bandit false positive ([3d62584](https://gitlab.com/notno/eeland/commit/3d62584e64c72e447c06d54f4027ec115e116032))
* **Dockerfile:** adds base image security updates ([247dcd7](https://gitlab.com/notno/eeland/commit/247dcd7169fc74727cf6a115ad4efe882db38ec0))
* switches to ci/templates beta ([8c90888](https://gitlab.com/notno/eeland/commit/8c9088886c93296092a6d96bb95a799f831eeafe))

## [0.1.13](https://gitlab.com/notno/eeland/compare/v0.1.12...v0.1.13) (2021-09-01)


### Bug Fixes

* restores original exception handling ([d385c7c](https://gitlab.com/notno/eeland/commit/d385c7c60f98402e5b7f88a4359ea7fdda9b4888))

## [0.1.12](https://gitlab.com/notno/eeland/compare/v0.1.11...v0.1.12) (2021-09-01)


### Bug Fixes

* handles root exception ([5eddad6](https://gitlab.com/notno/eeland/commit/5eddad671f94acc5ef7d96f8eeb5fc0e9f7877c6))
* updates dependencies ([3f0496e](https://gitlab.com/notno/eeland/commit/3f0496e25a2d680d5d77184f33672915cf31e66f))

## [0.1.11](https://gitlab.com/notno/eeland/compare/v0.1.10...v0.1.11) (2021-09-01)


### Bug Fixes

* fixes to versions and ci ([f825c27](https://gitlab.com/notno/eeland/commit/f825c27c2d68f22d89c68e48ca86acd63b04832e))
* run job only on main branch ([e3bd87b](https://gitlab.com/notno/eeland/commit/e3bd87ba633f504b2215f6faa2d33968fce2f2c3))

## [0.1.10](https://gitlab.com/notno/eeland/compare/v0.1.9...v0.1.10) (2021-09-01)


### Bug Fixes

* adds twine job ([2e8ec3c](https://gitlab.com/notno/eeland/commit/2e8ec3c5b3b29d9e27f598dd60a57669c0427c2e))

## [0.1.9](https://gitlab.com/notno/eeland/compare/v0.1.8...v0.1.9) (2021-09-01)


### Bug Fixes

* improves exception handling ([8c9f879](https://gitlab.com/notno/eeland/commit/8c9f879e28647a74ead3f93dd056b3ade2c935c5))

## [0.1.8](https://gitlab.com/notno/eeland/compare/v0.1.7...v0.1.8) (2021-09-01)


### Bug Fixes

* use the attribute instead of hardcoded value ([eabaeb5](https://gitlab.com/notno/eeland/commit/eabaeb588eb0e498994e80d637f832bd6aeb2d89))

## [0.1.7](https://gitlab.com/notno/eeland/compare/v0.1.6...v0.1.7) (2021-08-27)


### Bug Fixes

* lowers minimum required python version ([0667a5f](https://gitlab.com/notno/eeland/commit/0667a5f951d434da5e4363ae5102d924c20e902f))

## [0.1.6](https://gitlab.com/notno/eeland/compare/v0.1.5...v0.1.6) (2021-08-25)


### Bug Fixes

* reduce verbosity ([b542d45](https://gitlab.com/notno/eeland/commit/b542d459d9c4d7d8f30b5c49320a12a9b4633cbd))

## [0.1.5](https://gitlab.com/notno/eeland/compare/v0.1.4...v0.1.5) (2021-08-20)


### Bug Fixes

* prints only relevant portion of dataframe to logs ([777283e](https://gitlab.com/notno/eeland/commit/777283e2bc28fb25b0d4a47520e39658d1935690))

## [0.1.4](https://gitlab.com/notno/eeland/compare/v0.1.3...v0.1.4) (2021-08-20)


### Bug Fixes

* tries https then falls back to http ([99cca91](https://gitlab.com/notno/eeland/commit/99cca911da1dbb7fe96810b8481235ccee4f74d4))

## [0.1.3](https://gitlab.com/notno/eeland/compare/v0.1.2...v0.1.3) (2021-08-20)


### Bug Fixes

* better time handling ([dbcbc4b](https://gitlab.com/notno/eeland/commit/dbcbc4b6c03e71219f8185a88e467a0f2988fc05))

## [0.1.2](https://gitlab.com/notno/eeland/compare/v0.1.1...v0.1.2) (2021-07-02)


### Bug Fixes

* adds semantic-release config file ([ed729a4](https://gitlab.com/notno/eeland/commit/ed729a421522010d41b488000da4c4d297656a9e))
* corrects name of file ([8ac0a59](https://gitlab.com/notno/eeland/commit/8ac0a596dcc822559c4b9f906764eac2ed6e15b8))
* fixes python build ([1b30c52](https://gitlab.com/notno/eeland/commit/1b30c5269d6ebf18934b9b3f2285aa75b1040296))
* semantic release ([5b17ff8](https://gitlab.com/notno/eeland/commit/5b17ff8f91411fc5fdd4a84e36808824251cf39a))
* sets branches for which to run sem rel ([037709f](https://gitlab.com/notno/eeland/commit/037709ffa8ca1e0a6b0815bb86f48ea512f0a118))
